package com.Stock.daoImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.Stock.controller.StockController;
import com.Stock.dao.ValidateUserDao;
import com.Stock.exception.InvalidUserException;
import com.Stock.model.ValidateUser;
import com.Stock.util.ValidateUserRowMapper;

@Repository
public class ValidateUserDaoImpl implements ValidateUserDao {
	private static final Logger logger = LoggerFactory.getLogger(StockController.class);

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public String validateUser(ValidateUser validateUser) throws InvalidUserException {
		logger.info("Validating inSide ValidateUserDaoImpl");
		ValidateUser validateUserData = jdbcTemplate.queryForObject("select * from userdetails where userId=?",
				new Object[] { validateUser.getUserId() }, new ValidateUserRowMapper());
		String Status = "Success";
		if(validateUserData.getUserId()!=null && validateUserData.getPassword()!=null){
		try {
			if (validateUserData.getUserId().equals(validateUser.getUserId())
					&& validateUserData.getPassword().equals(validateUser.getPassword())) {

				Status = "success";
			} else {
			
				throw new InvalidUserException();
			}

		} catch (Exception e) {
			throw new InvalidUserException();
		}}
		else{
			System.out.println("NO Record Found");
		}
		logger.info("Validation successful inSide ValidateUserDaoImpl");
		return Status;

	}
}
