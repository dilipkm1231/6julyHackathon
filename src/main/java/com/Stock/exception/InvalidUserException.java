package com.Stock.exception;

public class InvalidUserException extends Exception {
		private static final long serialVersionUID = 6951505667383136924L;

		public InvalidUserException() {
			super("Not a Valid User");
		}
}
