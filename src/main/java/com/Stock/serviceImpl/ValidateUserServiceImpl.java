package com.Stock.serviceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Stock.controller.StockController;
import com.Stock.dao.ValidateUserDao;
import com.Stock.exception.InvalidUserException;
import com.Stock.model.ValidateUser;
import com.Stock.servcie.ValidateUserService;

@Service
public class ValidateUserServiceImpl implements ValidateUserService {
	private static final Logger logger = LoggerFactory.getLogger(StockController.class);

	@Autowired
	ValidateUserDao validateUserDao;

	@Override
	public String validateUser(ValidateUser validateUser) throws InvalidUserException {
		logger.info("Validating User ServiceImpl");
		return validateUserDao.validateUser(validateUser);

	}

}
