package com.Stock.util;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.Stock.model.ValidateUser;

public class ValidateUserRowMapper implements RowMapper<ValidateUser> {

	@Override
	public ValidateUser mapRow(ResultSet resultset, int arg1) throws SQLException {
		
		ValidateUser validateUser = new ValidateUser();
		validateUser.setUserId(resultset.getString("userId"));
		validateUser.setPassword(resultset.getString("password"));
		
		return validateUser;
	}


}
