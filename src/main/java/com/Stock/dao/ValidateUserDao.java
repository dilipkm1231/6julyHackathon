package com.Stock.dao;

import com.Stock.exception.InvalidUserException;
import com.Stock.model.ValidateUser;

public interface ValidateUserDao {

	 public String validateUser(ValidateUser validateUser) throws InvalidUserException;
	 

}
