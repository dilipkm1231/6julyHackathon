package com.Stock.servcie;

import com.Stock.exception.InvalidUserException;
import com.Stock.model.ValidateUser;

public interface ValidateUserService {

	//public String validateUser(String userId);
	public String validateUser(ValidateUser validateUser) throws InvalidUserException;
}
