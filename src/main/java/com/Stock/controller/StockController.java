package com.Stock.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.Stock.exception.InvalidUserException;
import com.Stock.model.ValidateUser;
import com.Stock.servcie.ValidateUserService;

@RestController
public class StockController {
	
	private static final Logger logger = LoggerFactory.getLogger(StockController.class);

	@Autowired
	ValidateUserService validateUserService;
	
	@RequestMapping(value = "/validatelogin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public String checkUser1( @RequestBody ValidateUser validateUser) throws InvalidUserException {
		logger.info("Validating User");
     System.out.println("inside......");
		return validateUserService.validateUser(validateUser);

	}

}
